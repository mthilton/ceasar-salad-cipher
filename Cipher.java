import java.util.*;
import java.io.*;

class Cipher {

  //Global Variables
  static char cha;
  static int key;
  static Scanner scan;
  static Scanner newScan;
  static String toCheck;
  static String input;
  static String output = "";
  static String inputfilename = "input.txt";
  static String outputfilename = "output.txt";

  //Main Method
  static public void main(String[] args) {



    //Checks to see if there are any arguments; if true then uses file IO for rncrypt/decrypt
    if(args.length > 0){
      try {
        String arg = args[0];
        key = Integer.parseInt(args[1]);
        BufferedReader reader = new BufferedReader(new FileReader(inputfilename));
        BufferedWriter writer = new BufferedWriter(new FileWriter(outputfilename));

        //Checks to see if the input arg is encrypt
        if(arg.toLowerCase().equals("encrypt")){
          encryptIO(reader, writer);
          System.out.println("Encryption done! Check output.txt for the encrypted message.");

        //Checks to see if the input arg is encrypt
      } else if(arg.toLowerCase().equals("decrypt")){
          decryptIO(reader, writer);
          System.out.println("decryption done! Check output.txt for the decrypted message.");

        //if neither arg was found, throws user into the terminal version of the program
        } else {
          System.out.println("Invalid argument! Sending you to the terminal version.");
        }
        reader.close();
        writer.close();
      }

      //Error Reporting
      catch(FileNotFoundException ex){
        System.out.println("Unable to open file '" + inputfilename + "'");
      }

      catch(IOException ex){
        System.out.println("Error reading or writing. Here is the Stack trace:");
        ex.printStackTrace();
      }

    } else {
      scan = new Scanner(System.in);
      System.out.println("Welcome to the Ceasar Salad Cipher!\nType 'D' for Decrypt, 'E' for Encrypt, or 'X' to exit:");

      //While Loop to take the first Input
      while(scan.hasNext()) {
        toCheck = scan.next();

        //Base Case: Exits the program
        if (toCheck.toLowerCase().equals("x")) {
          System.exit(0);

        //Checks to see if input is "e" or "E"; If true, runs Encryption Method.
        } else if (toCheck.toLowerCase().equals("e")) {
          encryptTerminal();
          output = "";
          System.out.println("\n\n\nType 'D' for Decrypt, 'E' for Encrypt, or 'X' to exit:");
          continue;

        //Checks to see if input is "d" or "D"; If true, runs Decryption Method.
        } else if (toCheck.toLowerCase().equals("d")) {
          decryptTerminal();
          output = "";
          System.out.println("\n\n\nType 'D' for Decrypt, 'E' for Encrypt, or 'X' to exit:");
          continue;

        //Error Checking: if there is not a valid input, we get this error message and we get to put a new char
        } else {
          System.out.println("Invalid Input!\n\nType 'D' for Decrypt, 'E' for Encrypt, or 'X' to exit:");
          continue;

        }
      }
      scan.close();
    }
  }


  //Encrypt Method
  static void encryptTerminal(){
    newScan = new Scanner(System.in);             //Creates a new Scanner Variable. This allows us to have a different variable for our input string
    System.out.println("\n\n*~Encrypt~*\n\n");
    System.out.println("Type the phrase you would like to encrypt:");
    input = newScan.nextLine();                   //Allows Us to pull the string input from the command line and store it as a variable
    getKey();
    encryptWhileLoop();
    System.out.println("\nHere is your encrypted message:");
    System.out.println(output);
    }

  //Decrypt Method
  static void decryptTerminal(){
    newScan = new Scanner(System.in);             //Creates a new Scanner Variable. This allows us to have a different variable for our input string
    System.out.println("\n\n*~Decrypt~*\n\n");
    System.out.println("Type the phrase you would like to decrypt:");
    input = newScan.nextLine();                   //Allows Us to pull the string input from the command line and store it as a variable
    getKey();
    decryptWhileLoop();
    System.out.println("\nHere is your encrypted message:");
    System.out.println(output);
  }

  //Method to get the key from the user.
  static void getKey(){
    System.out.println("\nEnter Desired Key:");
    key = Integer.parseInt(scan.next());        //This does several things. First, grabs a string from the console, then it typecasts the string into an int
    while(key<1 || key>25){                        //This Bool check checks to make sure that the key int is between 1 and 25 and throws an error if it is out of bounds.
      System.out.println("Input Error: Key is out of bound (1-25)\n\n");
      key = Integer.parseInt(scan.next());
      getKey();
    }
  }

  //Method that Encrypts each individual char and stores it in the output String
  static void encryptWhileLoop(){
    int i = 0;                                  //Declaring Index Variable for the while loop
    while(i < input.length()) {                 //While i is less than the length of the input string, this while loop will continue
      cha = input.charAt(i);                    //Cha is a char. this allows us to pull each char in the string and work with them individually
      if(cha < 65 || cha > 122 || (cha > 90 && cha < 97)) {       //Checks to see if the char cha is a valid letter. if false, the key is not added. Used to keep the fidelity of no-leter chars
        output += cha;                          //Adds cha to the the output string. No shift has been made since the char is not a letter
        i++;
        continue;
      }
      if(isLower(cha) == true){                 //Checks to see if cha is a lowercase letter. If true then we compare to the ascii val of lowercase z
        cha += key;                             //Adds the int val key to the ascii val cha. This is what shifts the Char based on the Key
        if(cha > 122){                          //Checks to see if the new ascii val is no longer a valid char. if true, we subtract the alphabet (26). This allows us to wrap around
          cha -= 26;
        }
        output += cha;                          //Adds cha to the the output string.
        i++;
        continue;
      } else {                                  //Does the same as the double if check above. The difference is that we are dealing with Upper Case Letters, therefore a different range of ascii values
        cha += key;
        if(cha > 90){
          cha -= 26;
        }
        output += cha;                          //Adds cha to the the output string.
        i++;
      }
    }
  }

  //Method that Decrypts each individual char and stores it in the output String
  static void decryptWhileLoop(){
    int i = 0;                                //Declaring Index Variable for the while loop
    while(i < input.length()) {               //While i is less than the length of the input string, this while loop will continue
      cha = input.charAt(i);                  //Cha is a char. this allows us to pull each char in the string and work with them individually
      if(cha < 65 || cha > 122 || (cha > 90 && cha < 97)) {       //Checks to see if the char cha is a valid letter. if false, the key is not subtracted. Used to keep the fidelity of no-leter chars
        output += cha;                          //Adds cha to the the output string.
        i++;
        continue;
      }
      if(isLower(cha) == true){               //Checks to see if cha is a lowercase letter. If true then we compare to the ascii val of lowercase z
        cha -= key;                           //Adds the int val key to the ascii val cha. This is what shifts the Char based on the Key
        if(cha < 97){                         //Checks to see if the new ascii val is no longer a valid char. if true, we add the the alphabet (26). This allows us to wrap around
          cha += 26;
        }
        output += cha;                        //Adds cha to the the output string.
        i++;
        continue;
      } else {                                //Does the same as the double if check above. The difference is that we are dealing with Upper Case Letters, therefore a different range of ascii values
        cha -= key;
        if(cha < 65){
          cha += 26;
        }
        output += cha;                          //Adds cha to the the output string.
        i++;
      }
    }
  }

  //Method to check to see if a given char "cha" is lowercase also given that it is a letter
  static boolean isLower(char cha) {
    if(cha > 96){
      return true;
    } else {
      return false;
    }
  }

  //Method to use an input file as the target for encryption
  static void encryptIO(BufferedReader reader, BufferedWriter writer) throws java.io.IOException{
    String line = null;
    while((input = reader.readLine()) != null){
      writer.newLine();
      encryptWhileLoop();
      writer.write(output);
      output = "";
    }
  }

  //Method to use an input file as the target for decryption
  static void decryptIO(BufferedReader reader, BufferedWriter writer) throws java.io.IOException{
    String line = null;
    while((input = reader.readLine()) != null){
      writer.newLine();
      decryptWhileLoop();
      writer.write(output);
      output = "";
    }
  }
}
