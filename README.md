#Ceasar Salad Cipher in Java
Author: Matthew Hilton

People that assisted: Adam Ames, Daniel Wright

----------------------------------------------------------------------------

##How to run

There are two ways to use this program:

1. In the directory that CeasarCipher.jar is in type:

```
java -jar CeasarCipher.jar
```

This will allow you to encrypt short sentences.

2. If you have a paragraph or several paragraphs that you would like to encrypt use:

```
java -jar CeasarCipher.jar <encrypt or decrypt> <key>
```

Note: Must edit the input.txt file before running this command. That is where it gets the paragraph(s) to encrypt/decrypt.

----------------------------------------------------------------------------

##Known Bugs

<Resolved!> Sometimes will double up on certain lines. Cause: unknown

----------------------------------------------------------------------------

##Upcoming Features

None So far, but open to suggestions

----------------------------------------------------------------------------

##Changelog

7-12-2017

- Completed the project. ver 1.0

1-3-2018

- Made a makefile to compress the files into a jarfile for easy sharing and execution
- Updated README file

13-3-2018

- Allows for full page encryption using input/output files
- Can still use the terminal for encryption
- Updated README to use proper formatting
